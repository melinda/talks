## Build Your Days Around Creativity
### It's an experiment. Try it. 

Remember to keep notes and share (I use Day One & Evernote)

## Research Day 1 (example: Thursday)

Thursdays and Fridays tend to work well for research or study, immersing yourself in domain knowledge.

Before You Begin

* Mentally divide your day in half before you arrive
* The first part of your day you'll immerse, research, study
* Save admin, email, and other tasks for the last half

When You Arrive

* Sketch or write an encapsulating statement for your desired outcome
* Either plan what research buckets you want to dive into, or just jump in and let it lead you
* Set a pomodoro timer when you begin for 25 minutes
* After each session take a short or long break
* Stop whenever it makes sense to stop -- like when you don't think you can absorb any more for that day

That evening

* Follow your normal routine in the evening
* Find a binaural beats app or file (search "pure binaural" on YouTube. There are also some apps in the iTunes store)
* Have a beer or take some wine (if you wish)
* Find a quiet spot — you can do this at bedtime or if you tend to wake up in the night that's a good time to try it
* Throw on the headphones and try this breathing technique while the beats do their work:

Meditation Exercise

* Eyes closed
* Imagine a tiny white dot in your minds eye straight ahead of you -- focus on it
* Take some full, deep cleansing breaths
* Taking a breath in over 6-8 counts
* Hold the breath the same count
* Release the breath same count
* Continue this 4-5 times then take one more cleansing breath
* Then allow breathing to normalize

## Research Day 2 (example: Friday)

Follow the same halved day routine as your research day 1. 

In the evening or the following morning, try this interval exercise routine.
The idea is to work hard enough that your mind is only focused on the body and breath.
Don't work too hard, you should feel relaxed afterward, not exhausted.

_Use the other technique if you have cardio-pulmonary issues or if you prefer not to do this one_

* Choose your favorite cardio exercise
* Make sure you have a heart rate monitor, wristband, etc
* Aim to reach your target heart rate [Here's a calculator](http://www.mayoclinic.com/health/target-heart-rate/SM00083)
* Try to stay at your target heart rate, at moderate difficulty, for 30 seconds or so, _don't let it go above your max_
* After 30 seconds at your target rate, ease up, get your heart rate back down
* Take deep breaths in and release the breath out as slowly as you can
* Do this until your heart rate lowers to your resting rate (near the bottom of you calclated range)
* Continue these intervals for as long as you wish

Have fun with it. experiment. Make room for unique, novel, useful and relevant ideas.


