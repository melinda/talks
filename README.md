# Creativity in The Material World

This talk was given at Charlotte UX Monday Aug 12, 2013. 
We had a great crowd, lots of questions and interaction. Fantastic!

## Questions, Feedback, Resources, or just sayin' "Hi"

If you have questions, challenges, ideas, feel free to submit an issue for discussion. 
I'd love to hear from you. Thanks for listening. (You must have an account on bitbucket submit issues.)

## Contributing

If you have a routine you'd like to contribute, clone the repo, add your routine and send a pull request. If you dunno how to, direct message me @melindavoo.

Melinda